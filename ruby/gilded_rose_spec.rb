require File.join(File.dirname(__FILE__), 'gilded_rose')

describe GildedRose do
  describe "#update_quality" do
    it "does not change the name" do
      items = [Item.new("foo", 0, 0)]
      GildedRose.new(items).update_quality()
      expect(items[0].name).to eq "foo"
    end
  end
end

describe ItemWrapper do
  include ItemTypes

  let(:item) { Item.new("Sample item", 15, 20) }
  subject { ItemWrapper.new(item) }
  let(:strategy) { subject.instance_variable_get(:@wrapped_item) }

  describe "#wrap_item" do
    it "wrap item" do
      expect(subject.instance_variable_get(:@item)).to equal item
    end
  end

  describe "#update_sell_in" do
    it "pass update_sell_in call to strategy class" do
      expect(strategy).to receive(:update_sell_in)
      subject.update_sell_in
    end

    it "return wrapper" do
      expect(subject.update_sell_in).to equal subject
    end
  end

  describe "#update_quality" do
    it "pass update_quality call to strategy class" do
      expect(strategy).to receive(:update_quality)
      subject.update_quality
    end

    it "return wrapper" do
      expect(subject.update_quality).to equal subject
    end
  end

  describe "#method_missing" do
    it "pass missing method call to strategy class" do
      expect(strategy).to receive(:test_method)
      subject.test_method
    end

    it "return wrapper" do
      allow(strategy).to receive(:test_method)
      expect(subject.test_method).to eq subject
    end
  end

  describe "#respond_to_missing?" do
    it "return true, if method is defined in the strategy class" do
      allow(strategy).to receive(:test_method)
      expect(subject.respond_to?(:test_method)).to eq true
    end

    it "return false, if method is not defined in the strategy class" do
      expect(subject.respond_to?(:test_method)).to eq false
    end
  end

  describe "#get_strategy_class" do
    it "return valid class for each custom item type" do
      ItemTypes::TYPES.each do |key, value|
        @strategy = ItemWrapper.new(
            Item.new("#{key} any name", 10, 10)
        ).instance_variable_get(:@wrapped_item)
        expect(@strategy).to be_an Object.const_get(value)
      end
    end

    it "return default class if type is not in the list" do
      @strategy = ItemWrapper.new(
          Item.new("Test any name", 10, 10)
      ).instance_variable_get(:@wrapped_item)
      expect(@strategy).to be_a DefaultItemType
    end
  end
end

describe DefaultItemType do

  let(:item) { Item.new("Sample item", 15, 20) }
  subject { DefaultItemType.new(item) }

  it "set item" do
    expect(subject.instance_variable_get(:@item)).to equal item
  end

  it "set max and min values for quality" do
    expect(subject.instance_variable_get(:@min_quality)).to be_an Integer
    expect(subject.instance_variable_get(:@max_quality)).to be_an Integer
  end

  describe "#update_quality" do
    it "decrease quality by one, if sell_in is more than 0" do
      item.instance_variable_set(:@sell_in, 4)
      item.instance_variable_set(:@quality, 4)
      subject.update_quality
      expect(item.instance_variable_get(:@quality)).to eq 3
    end

    it "decrease quality by two, if sell_in is 0" do
      item.instance_variable_set(:@sell_in, 0)
      item.instance_variable_set(:@quality, 4)
      subject.update_quality
      expect(item.instance_variable_get(:@quality)).to eq 2
    end
  end

  describe "#update_sell_in" do
    it "decrement sell_in by one if it is not less or equal 0" do
      item.instance_variable_set(:@sell_in, 2)
      subject.update_sell_in
      expect(item.instance_variable_get(:@sell_in)).to eq 1
    end

    it "does not set sell_in negative value" do
      item.instance_variable_set(:@sell_in, 0)
      subject.update_sell_in
      expect(item.instance_variable_get(:@sell_in)).to eq 0
    end
  end

  describe "#set_min_max_quality" do
    before(:each) do
      subject.instance_variable_set(:@min_quality, 123)
      subject.instance_variable_set(:@max_quality, 234)
      subject.send(:set_min_max_quality)
    end

    it "set min_quality" do
      expect(subject.instance_variable_get(:@min_quality)).to eq(0)
    end

    it "set max_quality" do
      expect(subject.instance_variable_get(:@max_quality)).to eq(50)
    end
  end

  describe "#quality_inc" do
    before(:each) do
      subject.instance_variable_set(:@min_quality, 0)
      subject.instance_variable_set(:@max_quality, 50)
    end
    it "increase value by 1 by default" do
      expect(subject.send(:quality_inc, 10)).to eq 11
    end

    it "increase value by passed value" do
      expect(subject.send(:quality_inc, 10, 20)).to eq 30
    end

    it "return min_value if sum is less then min_value" do
      expect(subject.send(:quality_inc, 10, -20)).to eq 0
    end

    it "return max_value if sum is more then max_value" do
      expect(subject.send(:quality_inc, 20, 35)).to eq 50
    end
  end

  describe "#quality_dec" do
    before(:each) do
      subject.instance_variable_set(:@min_quality, 0)
      subject.instance_variable_set(:@max_quality, 50)
    end

    it "decrease value by 1 by default" do
      expect(subject.send(:quality_dec, 10)).to eq 9
    end

    it "decrease value by passed value" do
      expect(subject.send(:quality_dec, 10, 5)).to eq 5
    end

    it "return min_value if sum is less then min_value" do
      expect(subject.send(:quality_dec, 10, 15)).to eq 0
    end

    it "return max_value if sum is more then max_value" do
      expect(subject.send(:quality_dec, 10, -150)).to eq 50
    end
  end

  describe "#undercut" do
    it "raise RuntimeError if min is more than max" do
      expect{subject.send(:undercut, 10, 10, 0)}.to raise_error(RuntimeError, "Max quality value must be equal or more then min.")
    end

    it "return min_value if sum is less then min_value" do
      expect(subject.send(:undercut, 15, 0, 10)).to eq 10
    end

    it "return max_value if sum is more then max_value" do
      expect(subject.send(:undercut, 0, 10, 15)).to eq 10
    end
  end
end

describe AgedBrieType do
  let(:item) { Item.new("Sample item", 15, 20) }
  subject { AgedBrieType.new(item) }

  describe "#update_quality" do
    it "increase quality by one" do
      item.instance_variable_set(:@quality, 4)
      subject.update_quality
      expect(item.instance_variable_get(:@quality)).to eq 5
    end
  end
end

describe SulfurasType do
  let(:item) { Item.new("Sample item", 15, 80) }
  subject { SulfurasType.new(item) }

  describe "#update_quality" do
    it "quality does not changing" do
      subject.update_quality
      expect(item.instance_variable_get(:@quality)).to eq 80
    end
  end
  describe "#update_sell_in" do
    it "sell_in does not changing" do
      subject.update_sell_in
      expect(item.instance_variable_get(:@sell_in)).to eq 15
    end
  end
end

describe ConjuredType do
  let(:item) { Item.new("Sample item", 15, 20) }
  subject { ConjuredType.new(item) }

  describe "#update_quality" do
    it "decrease quality by two, if sell_in is more than 0" do
      item.instance_variable_set(:@sell_in, 4)
      item.instance_variable_set(:@quality, 4)
      subject.update_quality
      expect(item.instance_variable_get(:@quality)).to eq 2
    end

    it "decrease quality by four, if sell_in is 0" do
      item.instance_variable_set(:@sell_in, 0)
      item.instance_variable_set(:@quality, 8)
      subject.update_quality
      expect(item.instance_variable_get(:@quality)).to eq 4
    end
  end
end

describe BackstagePassesType do
  let(:item) { Item.new("Sample item", 15, 20) }
  subject { BackstagePassesType.new(item) }

  describe "#update_quality" do

    it "increase quality by one, if sell_in is more than 10" do
      item.instance_variable_set(:@sell_in, 12)
      item.instance_variable_set(:@quality, 4)
      subject.update_quality
      expect(item.instance_variable_get(:@quality)).to eq 5
    end

    it "increase quality by two, if sell_in is more or equal 10" do
      item.instance_variable_set(:@sell_in, 9)
      item.instance_variable_set(:@quality, 4)
      subject.update_quality
      expect(item.instance_variable_get(:@quality)).to eq 6
    end

    it "increase quality by three, if sell_in is less than 6" do
      item.instance_variable_set(:@sell_in, 5)
      item.instance_variable_set(:@quality, 4)
      subject.update_quality
      expect(item.instance_variable_get(:@quality)).to eq 7
    end

    it "set quality to 0, if sell_in is 0" do
      item.instance_variable_set(:@sell_in, 0)
      item.instance_variable_set(:@quality, 4)
      subject.update_quality
      expect(item.instance_variable_get(:@quality)).to eq 0
    end
  end
end