module ItemTypes
  TYPES = {
      "Aged Brie" => "AgedBrieType",
      "Sulfuras" => "SulfurasType",
      "Backstage passes" => "BackstagePassesType",
      "Conjured" => "ConjuredType"
  }
end

class GildedRose
  def initialize(items)
    @items = items
  end

  def update_quality
    @items.each do |item|
      ItemWrapper.new(item)
          .update_sell_in
          .update_quality
    end
  end
end

class Item
  attr_accessor :name, :sell_in, :quality

  def initialize(name, sell_in, quality)
    @name = name
    @sell_in = sell_in
    @quality = quality
  end

  def to_s()
    "#{@name}, #{@sell_in}, #{@quality}"
  end
end

# This is a type of "wrapper" for item instances.
# You can describe any special strategy of processing for different types of items.
# To add a new type of items with a special behavior you'll need to:
#   1. Create class inherited from DefaultItemType and override a default behavior if needed.
#   2. Add an item's name beginning (indicator of type) as a key to the ItemTypes:TYPES hash;
#   3. Set created class name (type: string) as a value.
class ItemWrapper
  include ItemTypes

  def initialize(item)
    @item = item
    @wrapped_item = wrap_item(item)
  end

  # Definition of each method ease debugging, but aren't mandatory.
  def update_sell_in
    @wrapped_item.update_sell_in
    self
  end

  def update_quality
    @wrapped_item.update_quality
    self
  end

  def method_missing(name, *args, &block)
    @wrapped_item.send(name, *args, &block)
    self
  end

  def respond_to_missing?(name, include_private = false)
    @wrapped_item.respond_to?(name, include_private)
  end

  private

  def wrap_item(item)
    get_strategy_class(item).new(item)
  end

  # Get strategy class depends on item name beginning
  # @return [Class]
  def get_strategy_class(item)
    TYPES.each do |key, value|
      return Object.const_get(value) if item.name.start_with?(key)
    end
    DefaultItemType
  end
end

class DefaultItemType
  def initialize(item)
    @item = item
    set_min_max_quality
  end

  def update_quality
    @item.quality = @item.sell_in > 0 ? quality_dec(@item.quality) : quality_dec(@item.quality, 2)
  end

  def update_sell_in
    @item.sell_in = undercut(@item.sell_in.pred)
  end

  private

  # Override it in an inherited strategy class
  # if you want to set custom min and max values for a quality.
  def set_min_max_quality
    @min_quality = 0
    @max_quality = 50
  end

  def quality_inc(first, second=1)
    undercut(first + second, @min_quality, @max_quality)
  end

  def quality_dec(first, second=1)
    undercut(first - second, @min_quality, @max_quality)
  end

  def undercut(value, min=0, max=4294967295)
    if min > max
      raise "Max quality value must be equal or more then min."
    end
    return min if value < min
    return max if value > max
    value
  end
end

class AgedBrieType < DefaultItemType
  def update_quality
    @item.quality = quality_inc(@item.quality)
  end
end

class SulfurasType < DefaultItemType
  def update_quality
  end

  def update_sell_in
  end

  private

  def set_min_max_quality
    @min_quality = 80
    @max_quality = 80
  end
end

class BackstagePassesType < DefaultItemType
  def update_quality
    @item.quality = quality_inc(@item.quality, quality_inc_value_for(@item))
  end

  private

  def quality_inc_value_for(item)
    case item.sell_in
    when 0
      -1*item.quality
    when 6..10
      2
    when 1..5
      3
    else
      1
    end
  end
end

class ConjuredType < DefaultItemType
  def update_quality
    @item.quality = @item.sell_in > 0 ? quality_dec(@item.quality, 2) : quality_dec(@item.quality, 4)
  end
end